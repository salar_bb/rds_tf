# Create a RDS MySQL database instance in the VPC with our RDS subnet group and security group.

resource "aws_db_instance" "mysql_rds_db" {
  identifier           = "mydatabase"
  storage_type         = "gp2" #optional
  allocated_storage    =  20   #required
  engine               = "mysql"
  engine_version       = "8.0" #optional in terraform not in console
  instance_class       = "db.t2.micro"  #required
  port                 = "3306"
  name                 = "mydemoDB"
  username             =  var.username
  password             =  var.password
  parameter_group_name = "default.mysql8.0"
  availability_zone    = "us-east-1a"
  publicly_accessible  =  true
  replica_mode         =  open-read-only
  deletion_protection  =  true
  skip_final_snapshot  =  true

    tags = {
        Name    =   "Demomysqldb"
        Environment = "dev"
    }
}
